FROM alpine:3.10

ARG LOCAL_UID
ARG LOCAL_GID
ENV LOCAL_UID=$LOCAL_UID
ENV LOCAL_GID=$LOCAL_GID

RUN apk add --no-cache su-exec

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

WORKDIR /home/www/hoge

CMD /bin/ash
