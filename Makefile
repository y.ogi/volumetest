up:
	LOCAL_UID=`id -u` LOCAL_GID=`id -g` docker-compose up -d

down:
	docker-compose down

shell:
	docker-compose exec web /bin/ash

reset:
	rm -fr hoge
	docker container prune && docker rmi my/volumetest
