#!/bin/ash

USER_ID=${LOCAL_UID:-9001}
GROUP_ID=${LOCAL_GID:-9001}

echo "Starting with UID : $USER_ID, GID: $GROUP_ID"

addgroup -g ${GROUP_ID} -S www 
adduser -u ${USER_ID} -s /bin/ash -S www -G www 

exec /sbin/su-exec www "$@"
